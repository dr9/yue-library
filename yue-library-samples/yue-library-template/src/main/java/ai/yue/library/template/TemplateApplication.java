package ai.yue.library.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author  孙金川
 * @version 创建时间：2018年6月8日
 */
@SpringBootApplication
public class TemplateApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(TemplateApplication.class, args);
	}

}
