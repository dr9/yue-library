package ai.yue.library.data.redis.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author  孙金川
 * @version 创建时间：2018年9月11日
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QqUserDTO {

	String nickname;
	String figureurl_qq_1;
	Character gender;
	
}
