package ai.yue.library.base.constant;

/**
 * Token 常量
 * @author  孙金川
 * @version 创建时间：2017年10月8日
 */
public interface TokenConstant {

	/**
	 * Cookie Token Key
	 */
	public static final String COOKIE_TOKEN_KEY = "token";

	/**
	 * Redis Token 前缀
	 */
	public static final String REDIS_TOKEN_PREFIX = "token_%s";

	/**
	 * IP前缀
	 */
	public static final String IP_PREFIX = "ip_%s";
    
}
