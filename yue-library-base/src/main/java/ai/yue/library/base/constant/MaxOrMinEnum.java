package ai.yue.library.base.constant;

/**
 * 最大值 <i>或</i> 最小值
 * @author 	孙金川
 * @version 创建时间：2018年8月29日
 */
public enum MaxOrMinEnum {

	最大值, 
	最小值;

}
