package ai.yue.library.data.jdbc.constant;

/**
 * 排序方式
 * 
 * @author 	孙金川
 * @version 创建时间：2018年8月29日
 */
public enum DBSortEnum {

	降序, 
	升序;

}
